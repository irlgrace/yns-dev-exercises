<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            th, td {
                border-bottom: 1px solid #ddd;
                padding: 5px;
            }
            tr:hover {background-color:#f5f5f5;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &nbsp;
                </div>
            </div>
            <?php 
                $users = array();
                $file = fopen("users.csv","r");
                while (!feof($file)) { 
                    $user = fgetcsv($file);
                    if ($user) { 
                        $users[] = $user;
                    }
                }
                fclose($file);
            ?>
            <div class="row">
                <div class="col-md-12">
                    <h3 style="text-align:center;">List of Users</h3>
                    <table style="width:100%">
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>BirthDate</th>
                            <th>Sex</th>
                        </tr>
                        <?php
                            foreach ($users as $user) {
                                echo '<tr>'.
                                        '<td>'.$user[0].'</td>'.
                                        '<td>'.$user[1].'</td>'.
                                        '<td>'.$user[2].'</td>'.
                                        '<td>'.$user[3].'</td>'.
                                        '<td>'.$user[4].'</td>'.
                                        '<td>'.$user[5].'</td>'.
                                        '<td>'.$user[6].'</td>'.
                                    '</tr>';
                            } 
                        ?>
                    </table>
                </div>    
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>