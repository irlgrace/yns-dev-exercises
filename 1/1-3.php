<!DOCTYPE html>
<html>
    <head>
        <style>
            input[type=text] {
                width: 114px;
            }
            input[type=submit] {
                width: 238px;
            }
        </style>
    </head>
    <body>
        <form method="post">  
            <input type="text" name="number1"/>
            <input type="text" name="number2"/>
            <br/>
            <br/>
            <input  type="submit" name="gcf" value="Greatest Common Divisor">       
        </form>
        <br/> 
        <?php  
            if (isset($_POST['gcf'])) {
                $number1 = $_POST['number1'];  
                $number2 = $_POST['number2'];
                if (is_numeric($number1) && is_numeric($number2)) { 
                    $gcf = 1;
                    for ($i = 1; $i <= $number1 && $i <= $number2; ++$i) {
                        if (($number1 % $i == 0) && ($number2 % $i == 0)) {
                            $gcf = $i;
                        }  
                    }
                    echo 'Greatest Common Divisor:'.$gcf; 
                } else {
                    echo 'Invalid input';
                }   
            }
        ?> 
    </body>
</html>