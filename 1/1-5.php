<!DOCTYPE html>
<html>
    <head>
        <style>
            input[type=text] {
                width: 140px;
            }
            input[type=submit] {
                width: 238px;
            }
        </style>
    </head>
    <body> 
        <form method="post">  
            Last Number: <input type="date" name="date"/>
            <br/>
            <br/>
            <input type="submit" name="calculateDate" value="Calculate Date">       
        </form>
        <br/> 
        <?php  
            define("MAX_OF_3", 3);
            if (isset($_POST['calculateDate'])) {
                $date = $_POST['date'];
                if ($date == null) {
                    echo 'Please select a date';
                } else {
                    echo date('l Y-m-d', strtotime($date));
                    echo '<br/>';
                    for ($i = 1; $i < MAX_OF_3; $i++) {
                        echo date('l Y-m-d', strtotime($date. ' + '.$i.' days'));
                        echo '<br/>';
                    }
                }
            }
        ?> 
    </body>
</html>