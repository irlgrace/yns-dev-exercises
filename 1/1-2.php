<!DOCTYPE html>
<html>
    <head>
        <style>
            input[type=text] {
                width: 114px;
            }
        </style>
    </head>
    <body>
        <form method="post">  
            <input type="text" name="number1"/>
            <input type="text" name="number2"/>
            <br/>
            <br/>
            <input type="submit" name="add" value="Add">
            <input type="submit" name="subtract" value="Subtract">  
            <input type="submit" name="multiply" value="Multiply">  
            <input type="submit" name="division" value="Division">
        </form>
        <br/>  
        <?php  
            if (!empty($_POST)) {
                $number1 = $_POST['number1'];  
                $number2 = $_POST['number2'];
                if (is_numeric($number1) && is_numeric($number2)) { 
                    $result = 0;
                    if (isset($_POST['add'])) {  
                        $result =  $number1+$number2;     
                    }  
                    if (isset($_POST['subtract'])) { 
                        $result =  $number1-$number2;        
                    } 
                    if (isset($_POST['multiply'])) { 
                        $result =  $number1*$number2;        
                    }
                    if (isset($_POST['division'])) { 
                        $result =  $number1/$number2;        
                    }
                    echo 'Result: '.$result;  
                } else {
                    echo 'Invalid input';
                } 
            }
        ?> 
    </body>
</html>