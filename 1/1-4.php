<!DOCTYPE html>
<html>
    <head>
        <style>
            input[type=text] {
                width: 140px;
            }
            input[type=submit] {
                width: 238px;
            }
        </style>
    </head>
    <body>
        <form method="post">  
            Last Number: <input type="text" name="lastNumber"/>
            <br/>
            <br/>
            <input  type="submit" name="fizzBuzz" value="FizzBuzz Show">       
        </form>
        <br/> 
        <?php  
            if (isset($_POST['fizzBuzz'])) {
                $lastNumber = $_POST['lastNumber'];
                if (is_numeric($lastNumber)) {
                    if ($lastNumber > 0) {
                        for ($i = 1; $i <= $lastNumber; $i++) {
                            if ($i % 3 == 0 || $i % 5 == 0) {
                                if ($i % 3 == 0) {
                                    echo 'Fizz';
                                } 
                                if ($i % 5 == 0) {
                                    echo 'Buzz';
                                }   
                            } else {
                                echo $i;
                            }
                            echo '<br/>';
                        }
                    } else {
                        echo 'Number should be positive number';
                    }  
                } else {
                    echo 'Invalid input';
                } 
            }
        ?> 
    </body>
</html>