<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Fill Up User Details</h3>
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="firstName" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="middleName" placeholder="Middle Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="lastName" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <label>Birth Date</label>
                            <input type="date" class="form-control" name="birthDate">
                        </div>
                        <div class="form-group">
                            <label>Sex</label>  &nbsp;
                            <input type="radio" name="sex" value="male"/> Male
                            <input type="radio" name="sex" value="female"/> Female
                        </div>  
                        <div class="form-group">
                            <label>Select image to upload: </label>
                            <input type="file" name="userImage" />
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <?php  
                        //FUNCTION FOR VALIDATION
                        function checkValidEmail($email) {
                            return (!preg_match("/^[a-zA-Z0-9\.\_]+\@[a-zA-Z0-9]+\.[a-zA-Z0-9]{1,5}$/", $email)) ? false : true;
                        }
                        function checkValidUsername($username) {
                            return (!preg_match("/^[a-zA-Z0-9\.\_\-]+$/", $username)) ? false : true;
                        }
                        function checkValidName($name) {
                            return (!preg_match("/^[a-zA-Z\- ]+$/", $name)) ? false : true;
                        }
                        function checkValidBirthDate($birthDate) {
                            $dateNow = date("Y-m-d"); // this format is string comparable
                            return $birthDate > $dateNow ? false : true;
                        }
                      
                        if (isset($_POST['submit'])) {
                            $numberOfFields = 8;
                            $countEmpty = 0;
                            $error = '';
                            $username = $_POST['username'];  
                            $email = $_POST['email'];
                            $firstName = $_POST['firstName'];  
                            $middleName = $_POST['middleName'];
                            $lastName = $_POST['lastName'];
                            $birthDate = $_POST['birthDate'];
                            $sex = '';
                            $newImageName = '';
                            
                            if (empty($username)) {
                                $error .= 'Username is Required <br/>';
                                $countEmpty++;
                            } else {
                                if (!checkValidUsername($username)) {
                                    $error .= 'Username is not valid <br/>';
                                }
                            }
                            if (empty($email)) {
                                $error .= 'Email is Required <br/>';
                                $countEmpty++;
                            } else {
                                if (!checkValidEmail($email)) {
                                    $error .= 'Email is not valid <br/>';
                                }
                            }
                            if (empty($firstName)) {
                                $error .= 'First Name is Required <br/>';
                                $countEmpty++;
                            } else {
                                if (!checkValidName($firstName)) {
                                    $error .= 'First Name is not valid <br/>';
                                }
                            }
                            if (empty($middleName)) {
                                $error .= 'Middle Name is Required <br/>';
                                $countEmpty++;
                            } else {
                                if (!checkValidName($middleName)) {
                                    $error .= 'Middle Name is not valid <br/>';
                                }
                            }
                            if (empty($lastName)) {
                                $error .= 'Last Name is Required <br/>';
                                $countEmpty++;
                            } else {
                                if (!checkValidName($lastName)) {
                                    $error .= 'Last Name is not valid <br/>';
                                }
                            }
                            if (empty($birthDate)) {
                                $error .= 'Birth Date is Required <br/>';
                                $countEmpty++;
                            } else {
                                if (!checkValidBirthDate($birthDate)) {
                                    $error .= 'Birth Date is not valid <br/>';
                                }
                            }
                            if (!empty($_POST['sex'])) {
                                $sex = $_POST['sex'];     
                            } else {
                                $error .= 'Sex is Required <br/>';
                                $countEmpty++;
                            }
                            if (isset($_FILES['userImage']) && $_FILES['userImage']['error'] === UPLOAD_ERR_OK) {
                                $fileTmpPath = $_FILES['userImage']['tmp_name'];
                                $fileName = $_FILES['userImage']['name'];
                                $fileSize = $_FILES['userImage']['size'];
                                $fileType = $_FILES['userImage']['type'];
                                $fileNameCmps = explode(".", $fileName);
                                $fileExtension = strtolower(end($fileNameCmps));
                                $newFileName = md5(time() . $fileName) . '.' . $fileExtension;
                                $allowedExtensions = array('jpg', 'jpeg', 'png');
                                if (in_array($fileExtension, $allowedExtensions)) {
                                    $uploadFileDir = './image/';
                                    $dest_path = $uploadFileDir . $newFileName;      
                                    if (move_uploaded_file($fileTmpPath, $dest_path)) {
                                        $newImageName = $newFileName;
                                    } else {
                                        $error .= 'Error occured in uploading image';
                                    }
                                } else {
                                    $error .= "Invalid type for User Image <br/>"; 
                                }
                            } else {
                                $error .= 'Please Select an Image <br/>';
                            }    
                            if ($error == '') {
                                echo '<h3>User Details</h3>';
                                $list = array (
                                    array ($username, 
                                        $email, 
                                        $firstName, 
                                        $middleName, 
                                        $lastName, 
                                        $birthDate, 
                                        $sex,
                                        $newImageName
                                    )
                                );
                                if (0 == filesize("users.csv")) {
                                    $file = fopen("users.csv","w");
                                } else {
                                    $file = fopen("users.csv","a");
                                }    
                                foreach ($list as $line) {
                                    fputcsv($file, $line);
                                }
                                fclose($file);
                                echo 'user details added';
                            } else if ($countEmpty == $numberOfFields) {
                                echo '<h3>Error Messages</h3><br/>';
                                echo 'Please fill up all the fields.';
                            } else {
                                echo '<h3>Error Messages</h3><br/>';
                                echo $error; 
                            }
                        }
                    ?> 
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>