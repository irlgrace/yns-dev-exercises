<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            th, td {
                border-bottom: 1px solid #ddd;
                padding: 5px;
            }
            tr:hover {background-color:#f5f5f5;}
            a.active {
                background-color: #4287f5;
                color: #ffffff;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &nbsp;
                </div>
            </div>
            <?php 
                $perPage = 10;
                $pageCounter = 1;
                $next = $pageCounter + 1;
                $previous = $pageCounter - 1;
                $lastItem = $pageCounter * $perPage;

                if (isset($_GET['page'])) {
                    $pageCounter =  $_GET['page'];
                    $next = $pageCounter + 1;
                    $previous = $pageCounter - 1;
                    $lastItem = $pageCounter * $perPage;
                }
                $users = array();   
                $file = fopen("users.csv", "r");
                for ($iterate = 0; ($row = fgetcsv($file, 0, ",")) !== FALSE; $iterate++) {
                    if ($iterate < ($lastItem) && $iterate >= ($lastItem - $perPage)) {
                        $users[] = $row;
                    }
                }
                fclose($file);

                $forCountFile = file('users.csv', FILE_SKIP_EMPTY_LINES);
                $count = count($forCountFile);
                $paginations = ceil($count / $perPage);
            ?>
            <div class="row">
                <div class="col-md-12" style="text-align:center;">
                    <h3>List of Users</h3>
                    <table style="width:100%">
                        <tr>
                            <th>No.</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>BirthDate</th>
                            <th>Sex</th>
                            <th>Picture</th>
                        </tr>
                        <?php
                            foreach ($users as $i => $user) {
                                echo '<tr>'.
                                        '<td>'.(($perPage*$pageCounter - $perPage)+($i+1)).'</td>'.
                                        '<td>'.$user[0].'</td>'.
                                        '<td>'.$user[1].'</td>'.
                                        '<td>'.$user[2].'</td>'.
                                        '<td>'.$user[3].'</td>'.
                                        '<td>'.$user[4].'</td>'.
                                        '<td>'.$user[5].'</td>'.
                                        '<td>'.$user[6].'</td>'.
                                        '<td><img src="image/'.$user[7].'" width="50" /></td>'.
                                    '</tr>';
                            } 
                        ?>
                    </table>
                    <br/>
                    <div style="display:inline-block">
                        <ul class="pagination">
                            <?php
                                if ($pageCounter == 1) {
                                    echo "<li><a href=?page=$pageCounter class='active'>&nbsp;1&nbsp;</a></li>";
                                    for($j=2; $j <= $paginations; $j++) { 
                                        echo "<li><a href=?page=$j>&nbsp;".$j."&nbsp;</a></li> ";
                                    }
                                    if ($paginations > $pageCounter) {
                                        echo "<li><a href=?page=$next>&nbsp;Next&nbsp;</a></li> ";
                                    }
                                } else {
                                    echo "<li><a href=?page=$previous>Previous</a></li> "; 
                                    for ($j = 1; $j <= $paginations; $j++) {
                                        if ($j == $pageCounter) {
                                            echo "<li><a href=?page=$j class='active'>&nbsp;".$j."&nbsp;</a></li> ";
                                        } else {
                                            echo "<li><a href=?page=$j>&nbsp".$j."&nbsp</a></li> ";
                                        } 
                                    } 
                                    if($j != $pageCounter+1) {
                                        echo "<li><a href=?page=$next>&nbsp;Next &nbsp;</a></li> ";         
                                    }       
                                } 
                            ?>
                        </ul> 
                    </div>
                </div>    
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>