<?php
    session_set_cookie_params(0);
    session_start();
    if(isset($_SESSION['loginUser'])){
        header("location:users.php");
    } 
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    &nbsp;
                    <h3>Login</h3>
                    <form method="post">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        <small>Do you have an account here? <a href="register.php">Register here!</a></small>
                    </form>
                    <?php
                        if (isset($_POST['submit'])) {
                            $username = $_POST['username'];
                            $password = $_POST['password'];
                            $error = '';
                            $numberOfFields = 2;
                            $countEmpty = 0;

                            if (empty($username)) {
                                $error .= 'Username is Required.';
                                $countEmpty++;
                            } 
                            if (empty($password)) {
                                $error .= 'Password is Required.';
                                $countEmpty++;
                            }
                            if ($error=='') {
                                $user = array();
                                $match = false;
                                $file = fopen("users.csv","r");
                                while (!feof($file)) { 
                                    $user = fgetcsv($file);
                                    if ($user != null) { 
                                        if ($user[0] == $username && $user[1] == $password) {
                                            $match = true;
                                            $loginUser = array (
                                                'username' => $user[0],
                                                'password' => $user[1],
                                                'email' => $user[2],
                                                'firstName' => $user[3],
                                                'middleName' => $user[4],
                                                'lastName' => $user[5],
                                                'birthDate' => $user[6],
                                                'sex' => $user[7],
                                                'image' => $user[8]
                                            );
                                            break;
                                        } 
                                    }
                                }
                                fclose($file); 
                                if ($match == true) {
                                    $_SESSION['loginUser'] = $loginUser;
                                    header('location: users.php');
                                } else {
                                    echo "Incorrect username or password";
                                }
                            } else if ($countEmpty == $numberOfFields) {
                                echo 'Please fill up all the fields.';
                            } else {
                                echo $error; 
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>