<?php  
    session_start();

    function checkValidEmail($email) {
        return (!preg_match("/^[a-zA-Z0-9\.\_]+\@[a-zA-Z0-9]+\.[a-zA-Z0-9]{1,5}$/", $email)) ? false : true;
    }
    function checkValidUsername($username) {
        return (!preg_match("/^[a-zA-Z0-9\.\_\-]+$/", $username)) ? false : true;
    }
    function checkValidName($name) {
        return (!preg_match("/^[a-zA-Z\- ]+$/", $name)) ? false : true;
    }
    function checkValidBirthDate($birthDate) {
        $dateNow = date("Y-m-d"); // this format is string comparable
        return $birthDate > $dateNow ? false : true;
    }
        
    if (isset($_POST['submit'])) {
        $numberOfFields = 7;
        $countEmpty = 0;
        $error = '';
        $username = $_POST['username'];  
        $email = $_POST['email'];
        $firstName = $_POST['firstName'];  
        $middleName = $_POST['middleName'];
        $lastName = $_POST['lastName'];
        $birthDate = $_POST['birthDate'];
        $sex = '';
        if (empty($username)) {
            $error .= 'Username is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidUsername($username)) {
                $error .= 'Username is not valid <br/>';
            }
        }
        if (empty($email)) {
            $error .= 'Email is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidEmail($email)) {
                $error .= 'Email is not valid <br/>';
            }
        }
        if (empty($firstName)) {
            $error .= 'First Name is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidName($firstName)) {
                $error .= 'First Name is not valid <br/>';
            }
        }
        if (empty($middleName)) {
            $error .= 'Middle Name is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidName($middleName)) {
                $error .= 'Middle Name is not valid <br/>';
            }
        }
        if (empty($lastName)) {
            $error .= 'Last Name is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidName($lastName)) {
                $error .= 'Last Name is not valid <br/>';
            }
        }
        if (empty($birthDate)) {
            $error .= 'Birth Date is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidBirthDate($birthDate)) {
                $error .= 'Birth Date is not valid <br/>';
            }
        }
        if (!empty($_POST['sex'])) {
            $sex = $_POST['sex'];     
        } else {
            $error .= 'Sex is Required <br/>';
            $countEmpty++;
        }
        $message = '';
        if ($error == '') {
            $message .= '<h3>User Details</h3>';
            $message .= 'Username : '.$username.
            '<br/> Email : '.$email.
            '<br/> First Name : '.$firstName.
            '<br/> Middle Name : '.$middleName.
            '<br/> Last Name : '.$lastName.
            '<br/> Birth Date : '.$birthDate.
            '<br/> Sex : '.$sex;
        } else if ($countEmpty == $numberOfFields) {
            $message .= '<h3>Error Messages</h3><br/>'
                .'Please fill up all the fields.';
        } else {
            $message .= '<h3>Error Messages</h3><br/>'
                .$error;
        } 
        $_SESSION['message'] = $message;
        header("Location: form.php");
    }
?> 