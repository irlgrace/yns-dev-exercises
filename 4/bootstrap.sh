
#Install Apache
sudo yum install httpd -y
sudo service httpd start
sudo service iptables save

#Install PHP 5.6
wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
wget http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
sudo rpm -Uvh epel-release-6-8.noarch.rpm remi-release-6.rpm
sudo sed -i "s/mirrorlist=https/mirrorlist=http/" /etc/yum.repos.d/epel.repo
sudo yum install yum-utils -y
sudo yum-config-manager --enable remi-php56    [Intall PHP 5.6] -y
sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y

#Install MySQL 5.7
wget http://dev.mysql.com/get/mysql57-community-release-el6-7.noarch.rpm
sudo yum install mysql57-community-release-el6-7.noarch.rpm -y
sudo yum install mysql-community-client mysql-community-server -y
sudo service mysqld start
sudo grep -i temporary /var/log/mysqld.log

#Install Phpmyadmin
sudo yum install phpmyadmin -y
sudo sed -i '/Allow from 127.0.0.1/ c\ Allow from all' /etc/httpd/conf.d/phpMyAdmin.conf
sudo sed -i '/Allow from ::1/ d' /etc/httpd/conf.d/phpMyAdmin.conf
sudo service httpd restart

#Allowing ACCESS TO PORT
sudo iptables -I INPUT -p tcp --dport 80 -j ACCEPT #APACHE PORT
sudo iptables -A INPUT -p tcp --dport 443 -j ACCEPT #APACHE PORT
sudo iptables -A INPUT -p tcp --dport 3306 -j ACCEPT #MYSQL PORT
sudo service iptables save