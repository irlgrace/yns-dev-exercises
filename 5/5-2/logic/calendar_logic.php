<?php 
    $today = getDayToday();
    $monthToday = getMonthToday();
    $yearToday = getYearToday();
    $currentDay = getDayToday();
    $currentMonth = getMonthToday();
    $currentYear = getYearToday();
    $currentDayMonthFirstDay = getDayofTheFirstDay($currentMonth, $currentYear);
    $currentMonthLastDay = getLastDayoftheMonth($currentMonth,$currentYear);$currentMonthLastDay = getLastDayoftheMonth($currentMonth,$currentYear);
    $lastMonth = subtractMonth($currentMonth, $currentYear);
    $lastMonthYear = subtractMonthYear($currentMonth, $currentYear);
    $nextMonth = addMonth($currentMonth, $currentYear);
    $nextMonthYear = addMonthYear($currentMonth, $currentYear);
    
    if (isset($_GET['month']) && isset($_GET['year']) ) {
        $currentMonth = $_GET['month'];
        $currentYear = $_GET['year'];
        $currentDayMonthFirstDay = getDayofTheFirstDay($currentMonth, $currentYear);
        $currentMonthLastDay = getLastDayoftheMonth($currentMonth,$currentYear);
        $lastMonth = subtractMonth($currentMonth, $currentYear);
        $lastMonthYear = subtractMonthYear($currentMonth, $currentYear);
        $nextMonth = addMonth($currentMonth, $currentYear);
        $nextMonthYear = addMonthYear($currentMonth, $currentYear);
    }  
    
    function getDayToday() {
        return date("d");
    }

    function getMonthToday() {
        return date("m");
    }

    function getYearToday() {
        return date("Y");
    }

    function getMonthName($intMonth) {
        return date("F", mktime(0, 0, 0, $intMonth, 1, 0));
    }

    function getDayofTheFirstDay($intMonth, $intYear) {
        return date("N", mktime(0, 0, 0, $intMonth, 1, $intYear));
    }

    function getLastDayoftheMonth($currMonth, $currYear){
        return date("t", mktime(0, 0, 0, $currMonth, 1, $currYear));
    }

    function subtractMonth($currMonth, $currYear) {
        if($currMonth == 1) {
            $currYear--;
        }
        $lastmonth = mktime(0, 0, 0, $currMonth-1, 15, $currYear);
        return date("m", $lastmonth);
    }

    function subtractMonthYear($currMonth, $currYear) {
        if($currMonth == 1) {
            $currYear--;
        }
        return $currYear;
    }

    function addMonth($currMonth, $currYear) {
        if($currMonth == 12) {
            $currYear++;
        }
        $nextMonth = mktime(0, 0, 0, $currMonth+1, 15, $currYear);
        return date("m", $nextMonth);
    }

    function addMonthYear($currMonth, $currYear) {
        if($currMonth == 12) {
            $currYear++;
        }
        return $currYear;
    }
?>