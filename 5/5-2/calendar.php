<?php require_once "logic/calendar_logic.php"; ?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/design.css">     
    </head>
    <body>
        <div id='calendar'>
            <div id='calendar_header'>
                <?php
                    echo '<a href=?month='.$lastMonth.'&year='.$lastMonthYear.'><button id="previous">Previous</button></a>';
                    echo '<h1 id="month">'. getMonthName($currentMonth).' '.$currentYear.' </h1>';
                    echo '<a href=?month='.$nextMonth.'&year='.$nextMonthYear.'><button id="next">Next</button></a>' 
                ?>
            </div>
            <div id='calendar_body'>
                <table>
                    <tr>
                        <th class='calendar_col'>Sunday</th>
                        <th class='calendar_col'>Monday</th>
                        <th class='calendar_col'>Tuesday</th>
                        <th class='calendar_col'>Wednesday</th>
                        <th class='calendar_col'>Thursday</th>
                        <th class='calendar_col'>Friday</th>
                        <th class='calendar_col'>Saturday</th>
                    </tr>
                    <?php
                        $day = 1;
                        $incrementDay = false;
                        for($row = 0; $row < 6; $row++) {
                            echo '<tr class="calendar_row">';
                            for ($col = 0; $col < 7; $col++) {
                                if($day == 1){
                                    if($currentDayMonthFirstDay == $col || ($currentDayMonthFirstDay == 7 && $col == 0)){
                                        if($currentMonth == $monthToday && 
                                            $day == $today && 
                                            $currentYear == $yearToday) {
                                                echo '<td style="text-align:center; background-color:#98E0E8"><span><b>'.$day.'</b></span></td>';    
                                        } else {
                                            echo '<td style="text-align:center"><span>'.$day.'</span></td>';
                                        }
                                        $incrementDay = true;
                                    } else {
                                        echo '<td style="text-align:center"><span></span></td>';
                                    }
                                } else {
                                    if($day <= $currentMonthLastDay) {
                                        if($currentMonth == $monthToday && 
                                            $day == $today && 
                                            $currentYear == $yearToday) {
                                                echo '<td style="text-align:center; background-color:#98E0E8"><span><b>'.$day.'</b></span></td>';    
                                        } else {
                                            echo '<td style="text-align:center"><span>'.$day.'</span></td>';
                                        }
                                    } else {
                                        echo '<td style="text-align:center"><span></span></td>';
                                    }
                                }
                                if($incrementDay) {
                                    $day++;
                                }
                            }
                            echo '</tr>';   
                        } 
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>