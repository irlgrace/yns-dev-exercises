<!DOCTYPE html>
<html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/design.css"/>
    </head>
    <body>

        <h2 class='title'>EXERCISES</h2>
        <button type="button" class="collapsible">Exercise 1 - HTML & PHP</button>
        <div class="content">
            <ul>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-1.php' target="_blank">
                        <b>1-1</b> Show Hello World
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-2.php' target="_blank">
                        <b>1-2</b> The four basic operations of arithmetic
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-3.php' target="_blank">
                        <b>1-3</b> Show the greatest common divisor
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-4.php' target="_blank">
                        <b>1-4</b> Solve FizzBuzz problem
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-5.php' target="_blank">
                        <b>1-5</b> Input date. Then show 3 days from inputted date and its day of the week
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-6/form.php' target="_blank">
                        <b>1-6</b> Input user information. Then show it in next page
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-7/form.php' target="_blank">
                        <b>1-7</b> Add validation in the user information form(required, numeric, character, mailaddress)
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-8/form.php' target="_blank">
                        <b>1-8</b> Store inputted user information into a CSV file
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-9/users.php' target="_blank">
                        <b>1-9</b> Show the user information using table tags
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-10/form.php' target="_blank">
                        <b>1-10</b> Upload images
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-11/users.php' target="_blank">
                        <b>1-11</b> Show uploaded images in the table
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-12/users.php' target="_blank">
                        <b>1-12</b> Add pagination in the list page
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/1/1-13/login.php' target="_blank">
                        <b>1-13</b> Create login form and embed it into the system that you developed
                    </a>
                </li>
            </ul>  
        </div>
        <button type="button" class="collapsible">Exercise 2 - Javascript</button>
        <div class="content">
            <ul>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-1.php' target="_blank">
                        <b>2-1</b> Show alert
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-2/start.php' target="_blank">
                        <b>2-2</b> Confirm dialog and redirection
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-3.php' target="_blank">
                        <b>2-3</b> The four basic operations of arithmetic
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-4.php' target="_blank">
                        <b>2-4</b> Show prime numbers
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-5.php' target="_blank">
                        <b>2-5</b> Input characters in text box and show it in label
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-6.php' target="_blank">
                        <b>2-6</b> Press button and add a label below button
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-7/sample.php' target="_blank">
                        <b>2-7</b> Show alert when you click an image
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-8.php' target="_blank">
                        <b>2-8</b> Show alert when you click link
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-9.php' target="_blank">
                        <b>2-9</b> Change text and background color when you press buttons
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-10.php' target="_blank">
                        <b>2-10</b> Scroll screen when you press buttons
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-11.php' target="_blank">
                        <b>2-11</b> Change background color using animation
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-12/sample.php' target="_blank">
                        <b>2-12</b> Show another image when you mouse over an image. Then show the original image when you mouse out
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-13/sample.php' target="_blank">
                        <b>2-13</b> Change size of images when you press buttons
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-14/sample.php' target="_blank">
                        <b>2-14</b> Show images according to the options in combo box
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/2/2-15.php' target="_blank">
                        <b>2-15</b> Show current date and time in real time
                    </a>
                </li>
            </ul>
        </div>
        <button type="button" class="collapsible">Exercise 3 - Database</button>
        <div class="content">
            <ul>
                <li>
                    <a href='http://localhost/yns-dev-exercises/3/3-5/login.php' target="_blank">
                        <b>3-5</b> Use database in the applications that you developed
                    </a>
                </li>
            </ul>
        </div>
        <button type="button" class="collapsible">Exercise 5 - Practice</button>
        <div class="content">
            <ul>
                <li>
                    <a href='http://localhost/yns-dev-exercises/5/5-1/start.php' target="_blank">
                        <b>5-1</b> Quiz with three multiple choices
                    </a>
                </li>
                <li>
                    <a href='http://localhost/yns-dev-exercises/5/5-2/calendar.php' target="_blank">
                        <b>5-2</b> Calendar
                    </a>
                </li>
            </ul>
        </div>

        <script src="js/navigation_effect.js"></script>
    </body>
</html>