<?php
    require_once "logic/def.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/design.css">
    </head>
    <body>
        <div id='score_div'>
            <h1 id="score">
            <?php 
                $score = $_GET['score']; 
                echo $score; 
            ?>/10
            </h1>
            <p id="short-desc">
            <?php
                if ($score > 8) {
                    echo constant('EXCEPTIONAL');
                } else if ($score > 6) {
                    echo constant('VERY_GOOD');
                } else if ($score > 4) {
                    echo constant('GOOD');
                } else if ($score > 2) {
                    echo constant('FAIR');
                } else {
                    echo constant('POOR');
                }
            ?>
            </p>
            <a href="start.php"><button id="try_again">Try Again!</button></a>
        </div>
    </body>
</html>
