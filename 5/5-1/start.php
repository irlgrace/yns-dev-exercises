<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/design.css">
    </head>
    <body>
        <div id='start_div'>
            <h1 id="title">SciQuiz</h1>
            <p id="short-desc">This quiz is compose of random question what will test your knowledge about science.</p>
            <a href="quiz.php"><button id="start">Let's Start!</button></a>
        </div>
    </body>
</html>