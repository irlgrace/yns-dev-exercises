CREATE DATABASE quiz;

USE quiz;

CREATE TABLE questions (
	id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    question TEXT NOT NULL
);

CREATE TABLE choices (
	id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    question_id INT(11) UNSIGNED,
    choice_description TEXT NOT NULL,
    correct_answer BOOLEAN NOT NULL,
    FOREIGN KEY(question_id) REFERENCES questions(id)
);

INSERT INTO questions (question)
VALUES ('How many bones are there in a human body?'),
('Name the largest planet in the solar system.'),
('Name the most important element of the nuclear bomb?'),
('Leave a handful amount of coal at a very high temperature for a long time. What will you get?'),
('Nucleons are made up of nuetrons and?'),
('How many approximate days our moon takes to complete the orbit around the Earth?'),
('What is the name of the liquid component of the human blood?'),
('Name the person who studies animals?'),
('Which side of brain controls the left side of the human body?'),
('Which is the longest river on the Earth?'); 

INSERT INTO choices (question_id, choice_description, correct_answer)
VALUES 
(1,'205',false),
(1,'206',true),
(1,'207',false),
(2,'Jupiter',true),
(2,'Saturn',false),
(2,'Uranus',false),
(3,'Hydrogen',false), 
(3,'Uranium',true),
(3,'Xenon',false),
(4,'A gold',false),
(4,'A sapphire',false),
(4,'A diamond',true),
(5,'Electron',false),
(5,'Proton',true),
(5,'Proton and Electron',false),
(6,'30 days',true),
(6,'31 days',false),
(6,'365 days',false),
(7,'Plasma',true),
(7,'Red Blood Cell',false),
(7,'White Blood Cell',false),
(8,'Botanist',false),
(8,'Zoologist',true),
(8,'Biologist',false),
(9,'Right side',true),
(9,'Left side',false),
(9,'Lower side',false),
(10,'Amazon River',false),
(10,'Nile River',true),
(10,'Yellow River',false);


