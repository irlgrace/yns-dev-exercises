var q_num = 1;
var score = 0;
document.getElementById('q_num').textContent = q_num;

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    var index = 0;
    var questions;
    if (this.readyState == 4 && this.status == 200) {
        var data = xhttp.responseText;
        questions = JSON.parse(data);
        display (questions[index]);
        next(questions); 
    }
};
xhttp.open("GET", "http://localhost/yns-dev-exercises/5/5-1/logic/quiz_logic.php", true);
xhttp.send();

function display(question) {
    document.getElementById('question').textContent = question.question;
    var choices = document.getElementsByClassName('choice');
    choices[0].childNodes[1].textContent = question.choices[0].choice_description;
    choices[0].childNodes[0].value = question.choices[0].correct_answer;
    choices[1].childNodes[1].textContent = question.choices[1].choice_description;
    choices[1].childNodes[0].value = question.choices[1].correct_answer;
    choices[2].childNodes[1].textContent = question.choices[2].choice_description;
    choices[2].childNodes[0].value = question.choices[2].correct_answer;
}

function next(questions) {
    var next = document.getElementById('next');
    next.addEventListener("click", function(){
        var targetRadio =  document.querySelector('input[type=radio]:checked');
        var answer = 0;
        if (targetRadio) {
            answer = targetRadio.value;
        }
        score = +answer + +score;
        console.log(score);
        document.getElementById('q_num').textContent = q_num + 1;
        display(questions[q_num]);
        q_num++;
        if (q_num == 10) {
            document.getElementById('next_div').innerHTML = "<a href='end.php?score="+score+"'><button id='next'>Submit</button></a>" +
                "<small style='color:red'> <i> &nbsp; Reloading means restarting the quiz game.</i></small>";
        } 
    }); 
}

