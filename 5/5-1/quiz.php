<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/design.css">
    </head>
    <body>
        <div id="header">
            <h1 id="title_header">SciQuiz</h1>
        </div>
        <div id="quiz_div">
            <div id="question_div">
                <h3 id="question_num">Question <span id="q_num"></span></h3>
                <p id="question">Question</p>
            </div>
            <div id="choices_div">
                <div>
                    <br/>
                    <br/>
                    <form>
                    <p class="choice"><input type="radio" name="choice" value=""><span> Choice 1</span></p>
                    <br/>
                    <p class="choice"><input type="radio" name="choice" value=""><span> Choice 2</span></p>
                    <br/>
                    <p class="choice"><input type="radio" name="choice" value=""><span> Choice 3</span></p>
                    </form>
                </div> 
            </div>
            <div id="next_div">
                <button id='next'>Next</button>
                <small style="color:red"> <i> &nbsp; Reloading means restarting the quiz game.</i></small>
            </div>
        </div>
        <script src="js/quiz.js"></script>
    </body>
</html>