<?php
    require_once "config.php";
    
    $questions = array();
    $sql = "SELECT * FROM questions ORDER BY RAND()";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            
            $sqlForChoice = "SELECT * FROM choices ".
                        "WHERE question_id = ".$row['id'].
                        " ORDER BY RAND()";
            $resultChoices = $conn->query($sqlForChoice);
            $choices = array();
            if ($resultChoices->num_rows > 0) {
                while ($rowChoice = $resultChoices->fetch_assoc()) {
                    $choices[] = $rowChoice;
                }
            }
            $question = array('id'=>$row['id'],
                'question'=>$row['question'],
                'choices'=>$choices
            );
            $questions[] = $question;
        }
    }
    echo json_encode($questions);
    $conn->close();
?>