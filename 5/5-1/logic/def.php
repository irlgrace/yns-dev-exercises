<?php
    define("EXCEPTIONAL", "You're a genius!", true);
    define("VERY_GOOD", "Good Job!", true);
    define("GOOD", "Nice One!", true);
    define("FAIR", "Hmmm. That's fair enough!", true);
    define("POOR", "Please, study more!", true);
?>