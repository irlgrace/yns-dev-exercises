<!DOCTYPE html>
<html>
    <head>
        <style>
            #container {
                height: 1265px;
                position: relative;
            }
            #header {
                position: absolute;
                width: 100%;
                top: 10px;
            }
            #footer {
                position: absolute;
                width: 100%;
                bottom: 10px;
            } 
        </style>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <button onclick="scrollDown()">Scroll Down</button>
            </div>
            <div id="footer">
                <button onclick="scrollUp()">Scroll Up</button>
            </div>
        </div>
        <script type="text/javascript">
            function scrollDown () {  
                window.scrollTo(0,document.querySelector("#container").scrollHeight);
            }
            function scrollUp () {
                window.scrollTo(0,0);
            }
        </script>
    </body>
</html>