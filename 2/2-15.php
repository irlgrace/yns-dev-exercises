<!DOCTYPE html>
<html>
    <head>  
    </head>
    <body>
        <center>
        <h3 id="show"></h3>
        </center>
        <script type="text/javascript">
            var timer = setInterval(function(){
                var months = ["January", 
                        "February", 
                        "March", 
                        "April", 
                        "May", 
                        "June", 
                        "July", 
                        "August", 
                        "September", 
                        "October", 
                        "November", 
                        "December"
                ];
                var today = new Date();
                var date = months[today.getMonth()]+' '+today.getDate()+', '+today.getFullYear();
                var time;
                if (today.getHours()>12) {
                    time = (today.getHours()-12) + ":" + today.getMinutes() + ":" + today.getSeconds() + " PM";
                } else {
                    time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + " AM";
                }  
                var dateTime = date+' '+time;
                document.getElementById('show').innerHTML = dateTime;
            },1000); 
        </script>
    </body>
</html>