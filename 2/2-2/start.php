<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <center>
            <button onclick="confirmRedirect()">Click Me!</button>
        </center>
        <script type="text/javascript">
            function confirmRedirect() {
                var confirmation = confirm("Do you want to proceed?");

                if (confirmation == true) {
                    window.location.replace("redirect.php");
                }
            }
        </script>
    </body>
</html>