<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <input type="text" id="first-number" />   
        <small id="symbol">+</small>
        <input type="text" id="second-number" />
        <select id="operation" onchange="operation()" >
            <option value="add">Addition</option>
            <option value="subtract">Subtraction</option>
            <option value="multiply">Multiplication</option>
            <option value="divide">Division</option>
        </select>
        <button onclick="calculate()">Calculate</button>
        <br/> 
        <p>Result : <small id="answer"></small></p>
        <script type="text/javascript">
            function operation() {
                var operate = document.getElementById('operation').value;
                var symbol;
                switch (operate) {
                    case 'add': 
                        symbol = '+';
                        break;
                    case 'subtract':
                        symbol = '-';
                        break;
                    case 'multiply':
                        symbol = '*';
                        break;
                    case 'divide':
                        symbol = '/';
                        break;
                }
                document.getElementById('symbol').innerHTML = symbol;
            }
            function calculate() {
                var operate = document.getElementById('operation').value;
                var firstNum = document.getElementById('first-number').value;
                var secondNum = document.getElementById('second-number').value;
                if (firstNum && secondNum) {
                    if (isNaN(firstNum) || isNaN(secondNum)) {
                        alert('Please input number only.');
                    } else {
                        var result;
                        if (operate == 'add') {
                            result = +firstNum + +secondNum;
                        } else if (operate == 'subtract') {
                            result = firstNum - secondNum;
                        } else if (operate == 'multiply') { 
                            result = firstNum * secondNum;
                        } else if (operate == 'divide') {
                            result = firstNum / secondNum;
                        }
                        document.getElementById('answer').innerHTML = result;
                    }
                } else {
                    alert('Please fill in the text field/s');
                }
            } 
        </script>
    </body>
</html>