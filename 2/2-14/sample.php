<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <center>
            <h3>Some Breeds of Dog</h3>
            <img src="image/golden-retriever.jpg" height="250" id="image" />
            <br/>
            <select onchange="changeImage(this)">
                <option value="golden-retriever">Golden Retriever</option>
                <option value="siberian-husky">Siberian Husky</option>
                <option value="shiba">Shiba</option>
                <option value="german-shepherd">German Shepherd</option>
                <option value="samoyed">Samoyed</option>
            </select>
        </center>
        <script type="text/javascript">
            function changeImage(thisImage) {
                var breed = thisImage.value;
                var imageToChange = document.getElementById('image');
                switch (breed) {
                    case 'golden-retriever':
                        imageToChange.src="image/golden-retriever.jpg";
                        break;
                    case 'siberian-husky':
                        imageToChange.src="image/siberian-husky.jpg";
                        break;
                    case 'shiba':
                        imageToChange.src="image/shiba.jpg";
                        break;
                    case 'german-shepherd':
                        imageToChange.src="image/german-shepherd.jpg";
                        break;
                    case 'samoyed':
                        imageToChange.src="image/samoyed.jpg";
                        break;
                }
            }
        </script>
    </body>
</html>