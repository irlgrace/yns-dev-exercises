<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <center>
            <button onclick="changeImageSize(100)">Small</button>
            <button onclick="changeImageSize(250)">Medium</button>
            <button onclick="changeImageSize(500)">Large</button>
            <br/>
            <br/>
            <img src="image/tucker2.jpg" width="100" id="image" />
        </center>
        <script type="text/javascript">
            function changeImageSize(newWidth) {
                document.getElementById("image").width = newWidth;
            }
        </script>
    </body>
</html>