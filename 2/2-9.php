<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <center>
            <h1 class="text">Hello World</h1>
            <p class="text">Pick Text Color</p>
            <button onclick="changeTextColor('#f1bc31')">Yellow</button>
            <button onclick="changeTextColor('#71a95a')">Green</button>
            <button onclick="changeTextColor('#46b3e6')" >Blue</button>
            <button onclick="changeTextColor('#8b2f97')" >Purple</button> 
            <button onclick="changeTextColor('#7c0a02')">Red</button>
            <button onclick="changeTextColor('#fe6845')">Orange</button>
            <p class="text">Pick Background Color</p>
            <button onclick="changeBKColor('#f1bc31')">Yellow</button>
            <button onclick="changeBKColor('#71a95a')">Green</button>
            <button onclick="changeBKColor('#46b3e6')" >Blue</button>
            <button onclick="changeBKColor('#8b2f97')" >Purple</button> 
            <button onclick="changeBKColor('#7c0a02')">Red</button>
            <button onclick="changeBKColor('#fe6845')">Orange</button>
        </center>
        <script type="text/javascript">
            function changeTextColor(newcolor) {
                var count = document.querySelectorAll('.text').length;
                var i;
                for (i = 0; i < count; i++) {
                    document.getElementsByClassName('text')[i].style.color = newcolor;
                }
            }
            function changeBKColor(newcolor) {
                document.body.style.background = newcolor;
            }
        </script>
    </body>
</html>