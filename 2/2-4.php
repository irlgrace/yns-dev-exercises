<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <input type="text" id="number"/>  
        <button onclick="calculatePrimeNumber()">Show Prime Numbers</button> 
        <div id="primeNumbers">
        </div>
        <script type="text/javascript">
            function calculatePrimeNumber() {
                var div = document.getElementById('primeNumbers');
                div.innerHTML = '';
                var maxNumber = document.getElementById('number').value;
                if (isNaN(maxNumber)) {
                    div.innerHTML = '<p>Invalid Input</p>'
                } else {
                    if (maxNumber < 2) {
                        div.innerHTML = '<p>Please input number greater than 1</p>'
                    } else {
                        var counter;
                        for (counter = 2; counter <= maxNumber; counter++) {
                            var i;
                            var numOfFactor=0;
                            for (i=2; i < counter; i++) {
                                if (counter % i == 0) {
                                    numOfFactor++;
                                }
                            }
                            if (numOfFactor == 0) {
                                div.innerHTML += counter+'<br/>';
                            }
                        }
                    } 
                }
            } 
        </script>
    </body>
</html>