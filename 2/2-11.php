<!DOCTYPE html>
<html>
    <head>
    <style>
        body {
            background-color: #1E3F66;
        }
    </style>
    </head>
    <body>
        <center>
            <button onclick="changeColorByAnimation()">Lighten Background Color</button>
        </center>
        <script type="text/javascript">
            function changeColorByAnimation() {
                var bluecolor = ['#1E3F66','#2E5984','#528AAE','#73A5C6','#91BAD6','#BCD2E8'];
                var i = 0;
                var timer = setInterval(function(){
                    document.body.style.backgroundColor = bluecolor[i++];
                }, 100);
                
            }
        </script>
    </body>
</html>