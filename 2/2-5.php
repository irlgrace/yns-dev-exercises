<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <input type="text" id="inputText" oninput="writeText()"/>
        <label id="inputtedText"></label>
        <script type="text/javascript">
            function writeText() {
                var text = document.getElementById('inputText').value;
                document.getElementById('inputtedText').innerHTML = text;
            }
        </script>
    </body>
</html>