<?php
    require_once "config.php";
    
    if(!isset($_SESSION['loginUser'])){
        header("location:login.php");
        die();
    }   
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            th, td {
                border-bottom: 1px solid #ddd;
                padding: 5px;
            }
            tr:hover {background-color:#f5f5f5;}
            a.active {
                background-color: #4287f5;
                color: #ffffff;
            }
            .dropbtn {
                background-color: #4CAF50;
                color: white;
                padding: 5px;
                font-size: 16px;
                border: none;
            }
            .dropdown {
                position: relative;
                display: inline-block;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f1f1f1;
                min-width: 200px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }
            .dropdown-content a {
                color: black;
                padding: 10px 15px;
                text-decoration: none;
                display: block;
            }
            .dropdown-content a:hover {background-color: #ddd;}
            .dropdown:hover .dropdown-content {display: block;}
            .dropdown:hover .dropbtn {background-color: #3e8e41;}
            img {
                border-radius: 50%;
                width: 50px;
            }
            img.profile{
                width: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h6>Hi <?php echo $_SESSION['loginUser']['username']?>! Welcome! </h6>
                </div>
                <div class="col-md-6" >
                    <div style="position: absolute; right: 0;">
                        <div class="dropdown">
                            <button class="dropbtn"><img class="profile" src="image/<?php echo $_SESSION['loginUser']['image'];?>" style="border-radius:50%"/>
                            <?php echo $_SESSION['loginUser']['username'];?></button>
                            <div class="dropdown-content">
                                <a href="#">Name:  <?php echo $_SESSION['loginUser']['firstName'].' '.
                                                    $_SESSION['loginUser']['lastName'];?>
                                </a>
                                <a href="#">Birthday: <?php echo $_SESSION['loginUser']['birthDate'];?></a>
                                <a href="#">Sex: <?php echo $_SESSION['loginUser']['sex'];?></a>
                            </div>
                        </div>
                        <a href="logout.php"> Log Out </a>
                    </div>
                </div>
            </div>
            <?php 
                $perPage = 10;
                $pageCounter = 1;
                $next = $pageCounter + 1;
                $previous = $pageCounter - 1;
                $lastItem = $pageCounter * $perPage;
                $firstItem = $lastItem - $perPage;

                if (isset($_GET['page'])) {
                    $pageCounter =  $_GET['page'];
                    $next = $pageCounter + 1;
                    $previous = $pageCounter - 1;
                    $lastItem = $pageCounter * $perPage;
                    $firstItem = $lastItem - $perPage;
                }

                $users = array();
                $sqlForUsers = "SELECT * FROM users LIMIT $firstItem,$lastItem";
                $resultUsers = $conn->query($sqlForUsers);
                if ($resultUsers->num_rows > 0) {
                    while($row = $resultUsers->fetch_assoc()) {
                        $users[] = $row;
                    }
                } 
                
                $sqlForCountFile = "SELECT COUNT(*) FROM users";
                $result = $conn->query($sqlForCountFile);
                $resultForCount = $result->fetch_array();
                $count = $resultForCount[0];

                $conn->close();

                $paginations = ceil($count / $perPage);
            ?>
            <div class="row">
                <div class="col-md-12" style="text-align:center;">
                    <h3>List of Users</h3>
                    <table style="width:100%">
                        <tr>
                            <th>No.</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>BirthDate</th>
                            <th>Sex</th>
                            <th>Picture</th>
                        </tr>
                        <?php
                            $i = 1;
                            foreach ($users as $user) {
                                echo '<tr>'.
                                        '<td>'.(($perPage*$pageCounter - $perPage)+$i).'</td>'.
                                        '<td>'.$user['username'].'</td>'.
                                        '<td>'.$user['email'].'</td>'.
                                        '<td>'.$user['first_name'].'</td>'.
                                        '<td>'.$user['middle_name'].'</td>'.
                                        '<td>'.$user['last_name'].'</td>'.
                                        '<td>'.$user['birth_date'].'</td>'.
                                        '<td>'.$user['sex'].'</td>'.
                                        '<td><img src="image/'.$user['image'].'" /></td>'.
                                    '</tr>';
                                $i++;
                            } 
                        ?>
                    </table>
                    <br/>
                    <div style="display:inline-block">
                        <ul class="pagination">
                            <?php
                                if ($pageCounter == 1) {
                                    echo "<li><a href=?page=$pageCounter class='active'>&nbsp;1&nbsp;</a></li>";
                                    for($j=2; $j <= $paginations; $j++) { 
                                        echo "<li><a href=?page=$j>&nbsp;".$j."&nbsp;</a></li> ";
                                    }
                                    if ($paginations > $pageCounter) {
                                        echo "<li><a href=?page=$next>&nbsp;Next&nbsp;</a></li> ";
                                    }
                                } else {
                                    echo "<li><a href=?page=$previous>Previous</a></li> "; 
                                    for ($j = 1; $j <= $paginations; $j++) {
                                        if ($j == $pageCounter) {
                                            echo "<li><a href=?page=$j class='active'>&nbsp;".$j."&nbsp;</a></li> ";
                                        } else {
                                            echo "<li><a href=?page=$j>&nbsp".$j."&nbsp</a></li> ";
                                        } 
                                    } 
                                    if($j != $pageCounter+1) {
                                        echo "<li><a href=?page=$next>&nbsp;Next &nbsp;</a></li> ";         
                                    }       
                                } 
                            ?>
                        </ul> 
                    </div>
                </div>    
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>