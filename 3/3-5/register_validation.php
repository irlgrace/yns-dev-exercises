<?php
    require_once "config.php";
    
    if(isset($_SESSION['loginUser'])){
        header("location:users.php");
    } 
    echo 'here';
    $error = '';
    $finalError = '<h3>Error Messages</h3><br/>';
    $success = false;
    //FUNCTION FOR VALIDATION
    function checkValidEmail($email) {
        return (!preg_match("/^[a-zA-Z0-9\.\_]+\@[a-zA-Z0-9]+\.[a-zA-Z0-9]{1,5}$/", $email)) ? false : true;
    }
    function checkValidUsername($username) {
        return (!preg_match("/^[a-zA-Z0-9\.\_\-]+$/", $username)) ? false : true;
    }
    function checkValidName($name) {
        return (!preg_match("/^[a-zA-Z\- ]+$/", $name)) ? false : true;
    }
    function checkValidBirthDate($birthDate) {
        $dateNow = date("Y-m-d"); // this format is string comparable
        return $birthDate > $dateNow ? false : true;
    }
    function isUsernameUnique($username) {
        $isUnique = true;
        $sql = "SELECT * FROM users WHERE username = ?";
        $stmt = $GLOBALS['conn']->prepare($sql);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $isUnique = false;
        }   
        $stmt->close(); 
        return $isUnique;
    }

    if (isset($_POST['register_submit'])) {
        echo 'dito';
        $numberOfFields = 8;
        $countEmpty = 0;
        $username = trim($_POST['username']);  
        $password = trim($_POST['password']);
        $email = trim($_POST['email']);
        $firstName = trim($_POST['first_name']);  
        $middleName = trim($_POST['middle_name']);
        $lastName = trim($_POST['last_name']);
        $birthDate = $_POST['birth_date'];
        $sex = '';
        $newImageName = '';
        
        if (empty($username)) {
            $error .= 'Username is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidUsername($username)) {
                $error .= 'Username is not valid <br/>';
            }
        }
        if (empty($password)) {
            $error .= 'Password is Required <br/>';
            $countEmpty++;
        } else {
            if(strlen($password) < 5){
                $error .= "Password must have atleast 5 characters.";
            }
        }
        if (empty($email)) {
            $error .= 'Email is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidEmail($email)) {
                $error .= 'Email is not valid <br/>';
            }
        }
        if (empty($firstName)) {
            $error .= 'First Name is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidName($firstName)) {
                $error .= 'First Name is not valid <br/>';
            }
        }
        if (empty($middleName)) {
            $error .= 'Middle Name is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidName($middleName)) {
                $error .= 'Middle Name is not valid <br/>';
            }
        }
        if (empty($lastName)) {
            $error .= 'Last Name is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidName($lastName)) {
                $error .= 'Last Name is not valid <br/>';
            }
        }
        if (empty($birthDate)) {
            $error .= 'Birth Date is Required <br/>';
            $countEmpty++;
        } else {
            if (!checkValidBirthDate($birthDate)) {
                $error .= 'Birth Date is not valid <br/>';
            }
        }
        if (!empty($_POST['sex'])) {
            $sex = $_POST['sex'];     
        } else {
            $error .= 'Sex is Required <br/>';
            $countEmpty++;
        }    
        if (isset($_FILES['user_image']) && $_FILES['user_image']['error'] === UPLOAD_ERR_OK) {
            $fileTmpPath = $_FILES['user_image']['tmp_name'];
            $fileName = $_FILES['user_image']['name'];
            $fileSize = $_FILES['user_image']['size'];
            $fileType = $_FILES['user_image']['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));
            $newFileName = md5(time() . $fileName) . '.' . $fileExtension;
            $allowedExtensions = array('jpg', 'jpeg', 'png');
            if (in_array($fileExtension, $allowedExtensions)) {
                if ($fileSize > 2000000) {
                    $error .= "Image is too big, 2MB Max only.";
                } else {
                    $uploadFileDir = './image/';
                    $destPath = $uploadFileDir . $newFileName;
                }
            } else {
                $error .= "Invalid type for User Image <br/>"; 
            }
        } else {
            $error .= 'Please Select an Image <br/>';
        }
        $errorAgain = '';     
        if ($error == '') {
            if (isUsernameUnique($username)){
                if (move_uploaded_file($fileTmpPath, $destPath)) {
                    $newImageName = $newFileName;
                } else {
                    $errorAgain .= 'Error occured in uploading image';
                }
                if ($errorAgain == '') {
                    echo '<h3>User Details</h3>';
                    $newUser = array (
                            'username' => $username,
                            'password' => $password, 
                            'email' => $email, 
                            'firstName' => $firstName, 
                            'middleName' => $middleName, 
                            'lastName' => $lastName, 
                            'birthDate' => $birthDate, 
                            'sex' => $sex,
                            'image' => $newImageName
                    );
                    $sql = "INSERT INTO users (
                        username,
                        password,
                        email,
                        first_name,
                        middle_name,
                        last_name,
                        birth_date,
                        sex,
                        image
                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    $newBirthDate = date('Y-m-d', strtotime($birthDate));
                    if ($stmt = $conn->prepare($sql)) {
                        $stmt->bind_param("sssssssss",
                            $username,
                            $password,
                            $email,
                            $firstName,
                            $middleName,
                            $lastName,
                            $newBirthDate,
                            $sex,
                            $newImageName
                        );  
                        if ($stmt->execute()) {
                            $success = true;
                        } else {
                            $finalError .= 'Oops! Something went wrong.';
                        }
                    } 
                    $stmt->close();
                    // Close statement
                } else {
                    $finalError .= $errorAgain;
                }
            } else {
                $finalError .= 'Username already taken.';
            }
        } else if ($countEmpty == $numberOfFields) {
            $finalError .= 'Please fill up all the fields.'; 
        } else {
            $finalError .= $error;
        }
    }
    // Close connection
    $conn->close();
    if ($success) {
        $_SESSION['loginUser'] = $newUser;
        header('location: users.php');
    } else {
        $_SESSION['register_errors'] = $finalError;
        header('location: register.php');
    }
?>