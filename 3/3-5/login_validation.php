<?php
    require_once "config.php";
    
    if(isset($_SESSION['loginUser'])){
        header("location:users.php");
    } 
    
    if (isset($_POST['login_submit'])) {
        $finalError = '';
        $username = $_POST['username'];
        $password = $_POST['password'];
        $error = '';
        $numberOfFields = 2;
        $countEmpty = 0;

        if (empty($username)) {
            $error .= 'Username is Required.';
            $countEmpty++;
        } 
        if (empty($password)) {
            $error .= 'Password is Required.';
            $countEmpty++;
        }
        if ($error=='') {
            $sql = "SELECT * FROM users WHERE username = ? AND password = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ss",
                    $username, 
                    $password
                );
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            $conn->close();
            if ($result->num_rows > 0) {
                $user = $result->fetch_row();
                $loginUser = array (
                    'username' => $user[0],
                    'password' => $user[1],
                    'email' => $user[2],
                    'firstName' => $user[3],
                    'lastName' => $user[4],
                    'middleName' => $user[5],
                    'birthDate' => $user[6],
                    'sex' => $user[7],
                    'image' => $user[8]
                );
                $_SESSION['loginUser'] = $loginUser;
                header('location: users.php');
            } else {
                $finalError =  'Incorrect username or password';
            }
        } else if ($countEmpty == $numberOfFields) {
            $finalError =  'Please fill up all the fields.';
        } else {
            $finalError =  $error;
        }
        //Check for final error for login_validation
        if ($finalError != '') {
            $_SESSION['login_errors'] =  $finalError;
            header('location: login.php');
        }
    }
?>