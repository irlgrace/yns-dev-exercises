UPDATE employees SET birth_date = '1976-03-15' WHERE id = 1;
UPDATE employees SET birth_date = '1974-05-24', hire_date = '2014-04-01' WHERE id = 2;
UPDATE employees SET birth_date = '1990-08-13', hire_date = '2014-04-01' WHERE id = 3;
UPDATE employees SET birth_date = '1985-01-31', hire_date = '2014-12-01' WHERE id = 4;
UPDATE employees SET birth_date = '1983-10-11', hire_date = '2015-03-10' WHERE id = 5;
UPDATE employees SET birth_date = '1990-11-12', hire_date = '2015-02-15' WHERE id = 6;
UPDATE employees SET birth_date = '1993-09-13', hire_date = '2015-01-01' WHERE id = 7;
UPDATE employees SET birth_date = '1988-04-14', hire_date = '2015-04-10' WHERE id = 8;
UPDATE employees SET birth_date = '1983-07-15', hire_date = '2014-06-01' WHERE id = 9; 
