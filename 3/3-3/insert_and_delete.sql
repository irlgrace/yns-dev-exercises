INSERT INTO employees (
    first_name,
    last_name,
    middle_name,
    birth_date,
    department_id,
    hire_date,
    boss_id
) VALUES (
    'Abbie Jannina',
    'Oas',
    'S.',
    '1999-01-11',
    4,
    '2019-05-12',
    1
);

DELETE FROM employees WHERE id = 10;