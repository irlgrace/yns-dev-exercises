SELECT d.name, COUNT(e.id)
FROM employees e 
INNER JOIN
departments d
ON d.id = e.department_id
GROUP BY e.department_id;