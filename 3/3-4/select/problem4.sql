SELECT e.last_name AS Employee,
	  b.last_name AS Boss
FROM employees e
INNER JOIN employees b ON e.boss_id = b.id