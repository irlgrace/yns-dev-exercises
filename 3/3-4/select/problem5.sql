SELECT last_name FROM employees e, departments d 
WHERE e.department_id = d.id AND d.name = 'Sales'
ORDER BY last_name DESC;