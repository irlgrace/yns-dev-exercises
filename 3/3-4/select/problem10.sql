SELECT e.first_name, e.middle_name, e.last_name
FROM employees e
INNER JOIN employee_positions p ON e.id = p.employee_id
HAVING COUNT(e.id) > 2