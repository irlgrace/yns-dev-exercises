SELECT COUNT(middle_name) AS count_has_middle_name
FROM employees
WHERE NOT middle_name;