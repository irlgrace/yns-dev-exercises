SELECT id, 
    first_name, 
    last_name,
    middle_name,
    department_id,
    hire_date,
    boss_id
FROM employees 
WHERE last_name LIKE 'K%';