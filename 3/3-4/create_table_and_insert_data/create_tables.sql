CREATE TABLE departments (
    id INT(6) UNSIGNED AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE employees (
    id INT(6) UNSIGNED AUTO_INCREMENT,
    first_name VARCHAR(50),
    last_name VARCHAR(50) NOT NULL,
    middle_name VARCHAR(50),
    birth_date DATE NOT NULL,
    department_id INT(6) UNSIGNED NOT NULL,
    hire_date DATE,
    boss_id INT(6) UNSIGNED,
    PRIMARY KEY (id),
    FOREIGN KEY (department_id) REFERENCES departments(id)
);

CREATE TABLE positions (
    id INT(6) UNSIGNED AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE employee_positions (
    id INT(6) UNSIGNED AUTO_INCREMENT,
    employee_id INT(6) UNSIGNED NOT NULL,
    position_id INT(6) UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (employee_id) REFERENCES employees(id),
    FOREIGN KEY (position_id) REFERENCES positions(id)
);

/*GO TO alter_table.sql next*/